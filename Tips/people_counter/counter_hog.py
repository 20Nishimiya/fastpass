import numpy as np
import time
import cv2
video = cv2.VideoCapture("test0.mp4")
hog = cv2.HOGDescriptor((48,96), (16,16), (8,8), (8,8), 9)


while 1:
    ret, frame = video.read()
    orgHeight, orgWidth = frame.shape[:2]
    frame = cv2.resize(frame,  (int(orgWidth*300/orgHeight), 300))

    # Calculation of HoG feature quantity

    #Create Human Identifier with HoG feature quantity + SVM
    hog.setSVMDetector(cv2.HOGDescriptor_getDaimlerPeopleDetector())


    #widStride :Window movement amount
    #padding   :Extended range around the input image
    #scale     :scale
    hogParams = {'winStride': (8, 8), 'padding': (32, 32), 'scale': 1.05}

    #Detected person coordinate by the created identifier device 
    human, r = hog.detectMultiScale(frame, **hogParams)

    #Surround a person's area with a red rectangle
    for (x, y, w, h) in human:
        cv2.rectangle(frame, (x, y),(x+w, y+h),(0,50,255), 3)

    #show image
    cv2.imshow("results",frame)
    
    key = cv2.waitKey(3) & 0xFF
    if key == ord('q'):
        break
    
video.release()
cv2.destroyAllWindows()
