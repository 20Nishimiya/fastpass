import requests
from io import BytesIO
import matplotlib.pyplot as plt
from PIL import Image
import cv2

from staticmap import StaticMap

# 地図オブジェクトを生成
# 画像の横幅(ピクセル)と画像の縦幅(ピクセル)を指定
map = StaticMap(800, 600)

# 地図を描画した Pillow の PIL.Image オブジェクトを取得
# ズームレベルと経度・緯度を指定
image = map.render(zoom=17, center=[136.882090, 35.170560])

# 地図画像を保存
image.show()
