
|名称|セキュリティ|
| --- | --- |
|FTP（File Transfer Protocol）| - |
|FTPS(File Transfer Protocol over SSL/TLS)|「SSL/TLS」を利用して暗号化|
| SFTP（SSH File Transfer Protocol）| 「SSH（Secure Shell）」を利用して暗号化|
|SCP(Secure Copy Protocol)|「SSH」を利用。「SFTP」に比べて通信速度は速いといわれている|
