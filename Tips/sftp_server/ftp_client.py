# モジュールのインポート.
from ftplib import FTP

# FTP接続.
ftp = FTP("127.0.0.1", "user", passwd="password")

# ファイルのアップロード（テキスト）.
with open("ftp_test2.jpg", "rb") as f:  # 注意：バイナリーモード(rb)で開く必要がある
    ftp.storbinary("STOR /test.jpg", f)
