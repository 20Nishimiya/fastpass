#!/usr/bin/env python3
from flask import Flask, request, Response, abort, render_template
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin
import flask_login
from collections import defaultdict
import random

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
app.config['SECRET_KEY'] = "secret"



class UserData():
    class User(UserMixin):
        def __init__(self, id, name, password):
            self.id = id
            self.name = name
            self.password = password
        
        def __str__(self):
            return "Name = " + self.name + ", ID = " + str(self.id)
            
    def __init__(self):
        # self.users = {}
        self.users = {
            123: self.User(123, "u1", "p1"),
            456: self.User(456, "u2", "p2"),
            789: self.User(789, "u3", "p3")
        }

    def register(self, name, password):
        if name in [i.name for i in self.users.values()]:
            print("そのユーザー名は既に使われています")
            return "AlreadyUsedUserNameError"

        newId = random.random() # //TODO
        self.users[len(self.users)+1] = User(123, name, password)
        return "success"
        

    def getIDlist(self):
        return [x.id for x in self.users]

    #セキュリティの問題でIDが全て同じだとクッキ―データからUserを予測できる？とかの理由で一定時間ごとにリセットしたほうが良いらしい
    def changeUserId(self):
        pass

    def userVerify(self, name, password):
        print(id, password)
        for x in self.users.values():
            if x.name == name and x.password == password:
                return True
        return False

    def getUser(self, name):
        for x in self.users.keys():
            if self.users[x].name == name:
                print("ID = " + str(x))
                return self.users[x]
            return None

    def getUserFromID(self, id):
        if id in self.users.keys():
            return self.users[id]
        print("no user id =" + str(id))
        return False


# ログイン用ユーザー作成
users = UserData()


@login_manager.user_loader
def load_user(user_id):
    return users.getUserFromID(int(user_id))

@app.route('/')
def home():
    print(flask_login.current_user)
    text = "Hello, " + str(flask_login.current_user) + "<br><br>home: <a href='/login/'>Login</a>  \
            <a href='/protected/'>Protected</a> <a href='/logout/'>Logout</a>"
    return Response(text)

# ログインしないと表示されないパス
@app.route('/protected/')
@login_required
def protected():
    return Response('''
    protected<br />
    <a href="/logout/">logout</a>
    ''')

# ログインパス
@app.route('/login/', methods=["GET", "POST"])
def login():
    if(request.method == "POST"):
        # ユーザーチェック
        if users.userVerify(request.form["username"], request.form["password"]):
            # ユーザーが存在した場合はログイン
            login_user(users.getUser(request.form["username"]))
            return Response('''
            login success!<br />
            <a href="/protected/">protected</a><br />
            <a href="/logout/">logout</a>
            ''')
        else:
            return abort(401)
    else:
        return render_template("login.html")

# ログアウトパス
@app.route('/logout/')
@login_required
def logout():
    logout_user()
    return Response('''
    logout success!<br />
    <a href="/login/">login</a>
    ''')
    
    
if __name__ == "__main__":
    app.run(debug=True)

    