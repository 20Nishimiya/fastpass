import os
from flask import Flask, flash, request,Markup, abort, redirect, url_for, session
from werkzeug.utils import secure_filename

app = Flask(__name__)


@app.route('/')
def index():
    html = '''
    <form action="/test">
        <p><label>test: </label>
        <input type="text" name="query" value="default">
        <button type="submit" formmethod="get">GET</button>
        <button type="submit" formmethod="post">POST</button></p>
    </form>
    '''
    return Markup(html)


@app.route('/test', methods=['GET', 'POST'])
def test():
    # if request.method  is not 'POST': return abort(400)
    try:
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return abort(400) #redirect(request.url)
        else:
            file = request.files['file']
            file.save("download.jpg")
        print(request.form)
        return ""

    except Exception as e:
        return str(e)


if __name__ == '__main__':
    app.secret_key = 'hogehoge'
    app.run()
