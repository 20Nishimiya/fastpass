#! /usr/bin/python
import requests

if __name__ == "__main__":
    fileDataBinary = open("test.jpg", 'rb') #.read()
    files = {'file': ("test.jpg", fileDataBinary, "jpg")}

    url = 'http://127.0.0.1:5000/test'
    payload = {'name': '五木 鰤子',
        'gender': 'female',
        'query': 'win',
        'os': 'other',
        'location': 'asia'}
    r = requests.post(url, data=payload, files=files)
    print(r.text)
