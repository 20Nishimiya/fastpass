#! /usr/bin/python
import requests
import hashlib
import time

from counter import PointServer

if __name__ == "__main__":
    # points = [PointServer()]*7
    points = [
      
      PointServer("139", "35.0","国会議事堂", 1),
      PointServer("139", "35.1","国会議事堂2", 2),
      PointServer("139", "35.2","国会議事堂3", 3),
      PointServer("139", "35.3","国会議事堂4", 4),
      PointServer("139", "35.4","国会議事堂5", 5),
      PointServer("139", "35.5","国会議事堂6", 6),
      PointServer("139", "35.6","国会議事堂7", 7),
    ]

    last_send_time = time.time()
    while True:
      for p in points:
        p.updateWithoutSend()
      if time.time() - last_send_time > 1.0:
        print("SEND DATA!!!!")
        for p in points:
          time.sleep(0.05)
          p.sendData()
        last_send_time = time.time()
