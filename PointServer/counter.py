import numpy as np
import time
import cv2
import requests
import hashlib
import time


class PeopleCounter:
    def __init__(self):
        self.avg = None
        self.frame = None
        self.video = cv2.VideoCapture("../videos/1.mp4")
        self.count = 0

    def setIndex(self, num):
        self.video.release()
        cam_num = 1
        if 1<=int(num)<=7: cam_num = int(num)
        self.video = cv2.VideoCapture("../videos/" + str(cam_num) + ".mp4")
    def getImage(self):
        return self.frame

    def update(self):
        ret, self.frame = self.video.read()
        if ret == False:
            self.video.set(cv2.CAP_PROP_POS_FRAMES, 0)
            _, self.frame = self.video.read()

        flag = True
        text=""

        orgHeight, orgWidth = self.frame.shape[:2]
        self.frame = cv2.resize(self.frame,  (int(orgWidth*300/orgHeight), 300))

        # self.frame = imutils.resize(self.frame, width=500)
        self.gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        # _, self.gray = cv2.threshold(self.gray, 0, 255, cv2.THRESH_OTSU)
        self.gray = cv2.GaussianBlur(self.gray, (21, 21), 0)

        if self.avg is None:
            print("[INFO] starting background model...")
            self.avg = self.gray.copy().astype("float")

        cv2.accumulateWeighted(self.gray, self.avg, 0.1)
        self.frameDelta = cv2.absdiff(self.gray, cv2.convertScaleAbs(self.avg))
        # print(np.amax(self.frameDelta))
        if np.amax(self.frameDelta) < 70:
            self.count = 0
            return
        k = 5.0
        kernel = np.array([[-k, -k, -k], [-k, 1+8*k, -k], [-k, -k, -k]])
        self.frameDelta  = cv2.filter2D(self.frameDelta , ddepth=-1, kernel=kernel)
        self.frameDelta = cv2.GaussianBlur(self.frameDelta, (9, 9), 0)
        _, self.frameDelta = cv2.threshold(self.frameDelta, 0, 255, cv2.THRESH_OTSU)
        
        # self.frameDelta = cv2.threshold(self.frameDelta, 0, 255, cv2.THRESH_BINARY)[1]
        #thresh = cv2.dilate(thresh, None, iterations=3) # 膨張処理（物体に枠線を付ける感じ）
        #(cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_TREE , cv2.CHAIN_APPROX_SIMPLE)

        # self.frameDelta = cv2.dilate(self.frameDelta, None, iterations=3) # 膨張処理（物体に枠線を付ける感じ）
        (cnts, _) = cv2.findContours(self.frameDelta.copy(), cv2.RETR_TREE , cv2.CHAIN_APPROX_SIMPLE)
        self.c_tmp = 0
        x,y,w,h = 0,0,0,0
        for c in cnts:
            if 100 < cv2.contourArea(c) < 3000:
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(self.frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
                self.c_tmp += 1
        self.count = self.c_tmp

        
    def showImage(self):
        cv2.putText(self.frame, "In: {}".format(self.count), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 2)
        cv2.imshow("frame",self.frame)
        cv2.imshow("gray",self.gray)
        cv2.imshow("frameDelta",self.frameDelta)
        if cv2.waitKey(5) == 27:
            return False
        return True
        
        
    def quit(self):
        self.video.release()
        cv2.destroyAllWindows()


class PointServer:
    def __init__(self):
        self.name = ""
        self.user = ""
        self.password = ""
        self.coord = ["", ""]
        self.CAM = None

    def __init__(self, log, lat, name, index):
        self.name = ""
        self.user = ""
        self.password = ""
        self.coord = ["", ""]
        self.CAM = None
        self.setParams(log, lat, name, index)


    def setParams(self, log, lat, name, CamIndex):
        self.CAM = PeopleCounter()
        self.CAM.setIndex(CamIndex)
        self.log = log
        self.lat = lat
        self.user = "user0"
        self.password = "pass0"
        self.name = name

    def updateWithoutSend(self):
        self.CAM.update()

    def sendData(self):
        # fileDataBinary = open("test.jpg", 'rb').read()
        # files = {'file': ("test.jpg", fileDataBinary, "jpg")}

        ret, jpeg = cv2.imencode('.jpg', self.CAM.getImage())
        files = {'file': ("test.jpg", jpeg.tobytes(), "jpg")}

        url = 'http://127.0.0.1:5000/SendCamData'
        payload = {
            'user': self.user,
            'password': self.password,
            'log': self.log,
            'lat': self.lat,
            'name': self.name,
            'count': self.CAM.count,
            'crowded': self.CAM.count
        }
        r = requests.post(url, data=payload, files=files)
        print(r.text)


if __name__ is "__main__":
    c = PeopleCounter()
    loop = True
    while loop:
        c.update() 
        loop = c.showImage()

