# main.py

from all_data import *
import geocoder
from apikey import *
import requests, json

app = Flask(__name__)
login_manager = LoginManager()
login_manager.init_app(app)
app.config['SECRET_KEY'] = "secret"

users = UserData()
allData = Database()

@app.route("/getdata/", methods=['GET', 'POST'])
def getdata():
    if request.method != "GET": return ""
    dtype = request.args.get('type')
    if dtype == "MonitorCamList":
        mydict = allData.getCamClientList()

        # Serializing json    
        json_object = json.dumps(mydict)
        return str(json_object)

    elif dtype == "oneCamPos":
        _log = request.args.get('log')
        _lat = request.args.get('lat')
        mydict = allData.findOnePositionData(_lat, _log)

        print("####################################################")
        print("####################################################")
        print(_log, _lat)
        print(mydict)
        print("####################################################")
        print("####################################################")

        # Serializing json    
        json_object = json.dumps(mydict)
        return str(json_object)

    elif dtype == "searchPos":
        print(flask_login.current_user)
        query = request.args.get("pos")
        url = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
        r = requests.get(url + 'query=' + query + '&key=' + apiKey + "&language=ja") 
        res = r.json()['results'] 

        return_dict = {"points":[]}
        for i in res:
            x = dict(i)
            return_dict["points"].append({
                "name":i["name"],
                "lat":i["geometry"]["location"]["lat"], 
                "log":i["geometry"]["location"]["lng"], 
                "address":i["formatted_address"],
                "crowded":"undefinded",
                "type":i["types"][0] if len(i["types"]) > 0 else "Undefined",
                "cam_img_name":"undefinded",
                "runtime":i["opening_hours"] if "opening_hours" in i.keys() else "Undefined",
                "tel":"0120-444-555",
                "good_percent":"78%",
                "good_percent_2":4.5,
                "postal":"78%",
                "Runtime":"00:00 ~ 24:00",
                "crowded":"Very Crowded",
                "tel":"0120-444-555",
                "good_percent": str(float(i["user_ratings_total"]) * 20) +  "%",
                "good_percent_2": i["user_ratings_total"],
                "description":"Types = " + str(x["types"]) + "<br><br>business_status = " + str(x["business_status"])
            })

        return_dict["numPos"] = len(res)

        print("####################################################")
        print("####################################################")
        import pprint
        print("search QUERY = ", query)
        pprint.pprint(return_dict)
        print("####################################################")
        print("####################################################")
        return str(json.dumps(return_dict))


    elif dtype == "FacilitySearch":
        g= geocoder.osm(request.args.get("name"), maxRows= 100)
        return g.json


@app.route('/')
def index():
    data = {
        "address":"address_sample",
        "log":"123.456",
        "lat":"789.012",
        "type":"Restaurant",
        "Runtime":"00:00 ~ 24:00",
        "crowded":"Very Crowded",
        "tel":"0120-444-555",
        "good_percent":"78%",
        "good_percent_2":4.5
    }

    side_data = {
       "points": render_template("pointData.html", data = data),
       "List":"",
       "Route":"",
       "Favorite":""
    }
    print(side_data["points"])


    # text = "Hello, " + str(flask_login.current_user) + "<br><br>home: <a href='/login/'>Login</a>  \
    #         <a href='/protected/'>Protected</a> <a href='/logout/'>Logout</a>"
    # return Response(text)
    tmp__username = "Guest User"
    if flask_login.current_user.is_authenticated:
        tmp__username = flask_login.current_user.name

    favo_data = {
        "username" : tmp__username,
        "places"   : str("None")
    }
    return render_template('index.html', side_data = side_data, favo_data = favo_data)



@login_manager.user_loader
def load_user(user_id):
    return users.getUserFromID(int(user_id))



# ログインパス
@app.route('/login/', methods=["GET", "POST"])
def login():
    if(request.method == "POST"):
        # ユーザーチェック
        if not users.userVerify(request.form["username"], request.form["password"]): return abort(401)
            # ユーザーが存在した場合はログイン
        u_login = users.getUserObj(request.form["username"])
        if u_login is None: return abort(401)
       
        login_user(u_login)
        return redirect("/")
            
    else:
        return render_template("login.html")

    
@app.route("/register", methods=["GET", "POST"])
def register():
    if(request.method == "POST"):
        urname, passw = request.form["username"], request.form["password"]
        result = users.register(urname, passw)
        return result
    else:
        return render_template("register.html")


@app.route("/logout", methods=["GET", "POST"])
@login_required
def logout():
    logout_user()
    return redirect("/")


@app.route("/showInfo")
def showInfo():
    text = "<h1>Users</h1>"
    text += "<ul>"
    for i in users.users:
        text += f"<li>{i.id}, {i.name}, {i.password}</li>"
    text += "</ul>"

    text += "<h1>CameraDatas</h1>"
    text += str(allData.getCamClientList())
    print(text)
    return Response(text)


@app.errorhandler(404)
def not_found(e):
  return render_template('404error.html'), 404


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
import os

cam_users = [
    ["user0", "pass0"],
    ["user01", "pass01"],
    ["user02", "pass02"],
]

@app.route('/SendCamData',methods=["POST"])
def SendCamData():
    if 'file' not in request.files or request.files['file'].filename == '':
        print('No file part')
        return redirect("http://127.0.0.1:5000")
    data_dict = request.values
    if [data_dict["user"], data_dict["password"]] in cam_users:
        file = request.files['file']
        filename = "static/camimg/_" + str(data_dict["log"]) + "_"+ str(data_dict["lat"]) + ".jpg"
        print("image save --> " + filename)
        file.save(filename)
        allData.AddCamData(data_dict)
        return "thank you for sharing your data"
    return "404 invalid user"

# returnではなくジェネレーターのyieldで逐次出力。
# Generatorとして働くためにgenとの関数名にしている
# Content-Type（送り返すファイルの種類として）multipart/x-mixed-replace を利用。
# HTTP応答によりサーバーが任意のタイミングで複数の文書を返し、紙芝居的にレンダリングを切り替えさせるもの。
#（※以下に解説参照あり）

@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()), mimetype='multipart/x-mixed-replace; boundary=frame')

if __name__ == '__main__':
    app.run(debug=True)
    