#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/env python3
from flask import Flask, request, Response, abort, render_template
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin
import flask_login
from collections import defaultdict
import random
import cv2
import json   
from geopy.geocoders import Nominatim
from math import *
from apikey import *

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

        # Opencvのカメラをセットします。(0)はノートパソコンならば組み込まれているカメラ

    def __del__(self):
        self.video.release()

    def get_frame(self):
        success, image = self.video.read()
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

        # read()は、二つの値を返すので、success, imageの2つ変数で受けています。
        # OpencVはデフォルトでは raw imagesなので JPEGに変換
        # ファイルに保存する場合はimwriteを使用、メモリ上に格納したい時はimencodeを使用
        # cv2.imencode() は numpy.ndarray() を返すので .tobytes() で bytes 型に変換



class Database:
    def __init__(self):
        self.monitorPoints = []
        self.CamClients = []

    def CreatePointDic(self, log = 1000, lat=1000, name="NoneName", crowded = 0, runtime = "00:00~24:00", cam_img_name = "0.jpg"):
        loc_dict = {}
        if log == 1000 or lat == 1000:
            if name=="NoneName": print("error please input some information")
            geolocator = Nominatim(user_agent="test-dayo")
            location = geolocator.geocode(name)
            loc_dict = dict(location.raw)
        else:
            locator = Nominatim(user_agent="myGeocoder")
            location = locator.reverse(str(lat) + ", " + str(log))
            loc_dict = dict(location.raw)

        result = {
            "lat":loc_dict["lat"], 
            "log":loc_dict["lon"],
            "cam_img_name":"None",
            "address":loc_dict["display_name"],
            "crowded":crowded,
            "cam_img_name":cam_img_name,
            "runtime":runtime,
            "review":"78%",
            "description":"これはテストです"
        }

        if "type" in loc_dict.keys() and "class" in loc_dict.keys(): 
            result["class"] = loc_dict["type"] + " -- " + loc_dict["class"]
        else:
            result["class"] = "not identified"

        result["name"] = name
        return result

    def getCamClientList(self):
        my_dict = {"monitor_points":[]}
        for x in self.CamClients:
            my_dict["monitor_points"].append(x)
        return my_dict


    def AddCamData(self,data):
        camimg = "static/camimg/_" + str(data["log"]) + "_"+ str(data["lat"]) + ".jpg"
        p = self.CreatePointDic(log=data["log"], lat=data["lat"], crowded = data["crowded"], cam_img_name=camimg)
        p["name"] = data["name"]
        for i in range(len(self.CamClients)):
            if self.CamClients[i]["name"] == data["name"]:
                self.CamClients[i] = p
                return
        print("New Camera Clients!!")
        self.CamClients.append(p)

    def findOnePositionData(self, lat, log):
        for C in self.CamClients:
            if abs(float(C["log"]) - float(log)) < 0.01 and abs(float(C["lat"]) - float(lat)) < 0.01:
                return C
        return {}


def formatOnePointTableFromGooglePlaces(jsonObj, showDetail = false):
    # APIのreturnの例：
    # 'address_components': [色々],
    # 'adr_address': '<span class="country-name">日本</span>、<span '
    # 'class="postal-code">〒170-0002</span> <span '
    # 'class="region">東京都</span><span '
    # 'class="street-address">豊島区巣鴨２丁目１−１</span>',
    # 'business_status': 'OPERATIONAL',
    # 'formatted_address': '日本、〒170-0002 東京都豊島区巣鴨２丁目１−１',
    # 'formatted_phone_number': '03-5394-3471',
    # 'geometry': {'location': {'lat': 35.73385390000001, 'lng': 139.7385514},
    # 'viewport': {'northeast': {'lat': 35.73513373029149,
    #                 'lng': 139.7399361302915},
    # 'southwest': {'lat': 35.73243576970849,
    #                 'lng': 139.7372381697085}}},
    # 'icon': 'https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/shopping-71.png',
    # 'international_phone_number': '+81 3-5394-3471',
    # 'name': 'セブン-イレブン 巣鴨駅北口店',
    # 'opening_hours': {'open_now': True,
    # 'periods': [{'open': {'day': 0, 'time': '0000'}}],
    # 'weekday_text': ['月曜日: 24 時間営業',
    #         '火曜日: 24 時間営業',
    #         '水曜日: 24 時間営業',
    #         '木曜日: 24 時間営業',
    #         '金曜日: 24 時間営業',
    #         '土曜日: 24 時間営業',
    #         '日曜日: 24 時間営業']},
    # 'photos': [{'height': 3208,
    #     'html_attributions': ['<a href="https://maps.google.com/maps/contrib/107804304792480228126">さへ きひろのぶ</a>'],
    #     'photo_reference': 'ATtYBwJi0uESoMRravxRoqVJ9P28HBP29jP7Tagajt1lMDxxPWBmh6nyekK9DBtk55ytPxRCm47i1fKutMaRpY0EZ4sfTxYgGus5RpOz5_m7LS-p2QyweorncNUX2yuHim4xQMIXS1NLviP-hXUE1JYkzhrAqSPLE8Bpo6k-IAEr9c-ii9iz',
    #     'width': 4278},
    #     （複数)
    # ],
    # 'place_id': 'ChIJx3VTaJmNGGARfeDj6MylgCA',
    # 'plus_code': {'compound_code': 'PPMQ+GC 日本、東京都豊島区','rating': 3,'reference': 'ChIJx3VTaJmNGGARfeDj6MylgCA',
    # 'reviews': [{'author_name': 'kiai',
    #     'author_url': 'https://www.google.com/maps/contrib/101731714753678198564/reviews',
    #     'language': 'ja',
    #     'profile_photo_url': 'https://lh3.googleusercontent.com/a-/AOh14Gj3HVUbHUxhDbVPt2DHAWef3OkfGbjsKcG06ECP=s128-c0x00000000-cc-rp-mo-ba3',
    #     'rating': 2,
    #     'relative_time_description': '4 週間前',
    #     'text': '狭い上に客数は多いので、あまり清潔感がないです。(掃除をする余裕がない程に客数が多い)\n'
    #     'ATMも大勢の人が使うせいか、ATM利用明細がよく地面に落ちてます。',
    #     'time': 1611678237},（略）
    # ],
    # 'types': ['convenience_store','food','point_of_interest','store','establishment'],
    # 'url': 'https://maps.google.com/?cid=2342054105731817597',
    # 'user_ratings_total': 30,
    # 'utc_offset': 540,
    # 'vicinity': '豊島区豊島区巣鴨２丁目１−１',
    # 'website': 'http://www.sej.co.jp/'}
    # }

    return {
        "name":jsonObj["name"],
        "lat":jsonObj["geometry"]["location"]["lat"], 
        "lon":jsonObj["geometry"]["location"]["lng"], 
        "address":jsonObj["formatted_address"],
        "crowded":"undefinded",
        "place"
        "type":jsonObj["types"][0] if len(jsonObj["types"]) > 0 else "Undefined",
        "cam_img_name":"undefinded",
        "runtime":jsonObj["opening_hours"] if "opening_hours" in jsonObj.keys() else "Undefined",
        "tel":jsonObj["formatted_phone_number"],
        "good_percent": str(float(jsonObj["user_ratings_total"]) * 20) +  "%",
        "good_percent_2": jsonObj["user_ratings_total"],
        "postal":"78%",
        "url" : jsonObj["website"] if "website" in jsonObj.keys() else "No website",
    }


def getJsonDataFromPlaceID(place_id):
    import requests, json 
    url = "https://maps.googleapis.com/maps/api/place/details/json?"
    place_id = "ChIJx3VTaJmNGGARfeDj6MylgCA"

    r = requests.get(url + 'place_id=' + place_id + '&key=' + apiKey + "&language=ja") 
    x = r.json() 
    y = x['result'] 
    import pprint
    pprint.pprint(y)

    import sys
    sys.exit()


    # keep looping upto length of y 
    for i in range(len(y)): 
        print(y[i])
        # Print value corresponding to the 
        # 'name' key at the ith index of y 
        # print(y[i]['name']) 




class UserData():
    class User(UserMixin):
        def __init__(self, id, name, password):
            self.id = id
            self.name = name
            self.password = password
            self.Favorites = [] # [log, lat]のリスト
        
        def __str__(self):
            return f"Name = {self.name}, ID = {self.id}, password={self.password}\n Favorite places = {self.Favorites}"
            
    def __init__(self):
        # self.users = {}
        self.users = [
            self.User(123, "u1", "p1"),
            self.User(456, "u2", "p2"),
            self.User(789, "u3", "p3")
        ]

    def register(self, name, password):
        if name in [i.name for i in self.users]:
            print("そのユーザー名は既に使われています")
            return "AlreadyUsedUserNameError"

        newId = int(random.random()*1000) # //TODO
        self.users.append(self.User(newId, name, password))
        return "success"
        

    def getIDlist(self):
        return [x.id for x in self.users]

    #セキュリティの問題でIDが全て同じだとクッキ―データからUserを予測できる？とかの理由で一定時間ごとにリセットしたほうが良いらしい
    def changeUserId(self):
        pass

    def userVerify(self, name, password):
        print(id, password)
        for x in self.users:
            if x.name == name and x.password == password:
                return True
        return False

    def getUserID(self, name):
        for x in self.users:
            if x.name == name:
                print("ID = " + str(x.id))
                return x
        return None

    def getUserObj(self, name):
        for x in self.users:
            if x.name == name:
                print("ID = " + str(x.id))
                return x
        return None


    def getUserFromID(self, id):
        for x in self.users:
            if x.id == id: return x
        print("no user id =" + str(id))
        return False

